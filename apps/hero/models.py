# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from ..power.models import Power
from django.db import models

# Create your models here.
# class Power(models.Model):
#     name = models.CharField(max_length=100)
#     power_level = models.IntegerField()

class HeroManager(models.Manager):
    def create_valid(self, postdata):
        """
        Returns tuple: (is_valid, [errors])
        """
        errors = []
        is_valid = True 
        if len(postdata['name']) < 1:
            errors.append("Name field is required")
            is_valid = False
        if is_valid:
            self.create(
                name = postdata['name']
            )
        return (is_valid, errors)

class Hero(models.Model):
    name = models.CharField(max_length=100)
    powers = models.ManyToManyField(Power, related_name="heros")
    objects = HeroManager()
    def __str__(self):
        return self.name