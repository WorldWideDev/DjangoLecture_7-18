# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Hero
from ..power.models import Power
from django.contrib import messages
from django.shortcuts import render, redirect, reverse, HttpResponseRedirect

# Create your views here.
def index(req):
    context = {
        'heroes': Hero.objects.all()
    }
    return render(req, "hero/index.html", context)

def show(req, id):
    context = {
        "hero": Hero.objects.get(id=id),
        "powers": Power.objects.all()
    }
    return render(req, 'hero/show.html', context)

def create(req):
    valid = Hero.objects.create_valid(req.POST)
    if not valid[0]:
        for error in valid[1]:
            messages.error(req, error)
    return HttpResponseRedirect(reverse("heroes:index"))

def add_power(req, hero_id):
    the_hero = Hero.objects.get(id=hero_id)
    the_power = Power.objects.get(id=req.POST['power'])
    the_hero.powers.add(the_power)
    return HttpResponseRedirect(reverse("heroes:index"))