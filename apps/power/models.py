# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Power(models.Model):
    name = models.CharField(max_length=100)
    power_level = models.IntegerField()
    #heroes => [Hero]

    def __str__(self):
        return self.name