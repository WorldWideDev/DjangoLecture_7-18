# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Power
from django.shortcuts import render, HttpResponseRedirect, reverse

# Create your views here.
def index(req):
    context = {
        'powers': Power.objects.all()
    }
    return render(req, 'power/index.html', context)

def create(req):
    Power.objects.create(
        name=req.POST['name'],
        power_level=req.POST['level']
    )
    return HttpResponseRedirect(reverse("powers:index"))
def show(req, id):
    context = {
        "power": Power.objects.get(id=id)
    }
    return render(req, 'power/show.html', context)
